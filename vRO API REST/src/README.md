# Start my workflow "Sync all subscribed librarys in all vCenter" defined in VMware vRO

* with debug:

```C:\> perl start_vro_workflow.pl --id f14231dd-222f-4e4e-adb4-316cb854d57a --debug```

* without debug:

```C:\> perl start_vro_workflow.pl --id f14231dd-222f-4e4e-adb4-316cb854d57a```

The file "content_request.json" contains the body http request example in JSON format. 
Default content request without parameters:

```
{ "parameters": 
  [
  ]
}
```

If workflow does not have the input parameters then is not necessary to specify content request option 
in command line.

# Content request example for workflow what require input parameters:

```
{ "parameters": 
  [
    {
      "value": { "string": { "value": "myvc.mydomain.local" }},
      "type": "string",
      "name": "psServer",
      "scope": "local"
    },
    {
      "value": { "string": { "value": "myuser@mydomain.local" }},
      "type": "string",
      "name": "psUser",
      "scope": "local"
    },
    {
      "value": { "string": { "value": "mypassword" }},
      "type": "SecureString",
      "name": "psPassword",
      "scope": "local"
    },
    {
      "value": { "string": { "value": "DEVAUTOMATION" }},
      "type": "string",
      "name": "ds_prefix",
      "scope": "local"
    },
    {
      "value": { "string": { "value": "T3" }},
      "type": "string",
      "name": "tier",
      "scope": "local"
    }
  ]
}
```

# To run script you need to install the following Perl modules:

**[Aspect](http://search.cpan.org/CPAN/authors/id/A/AD/ADAMK/Aspect-0.98.tar.gz)** (version 0.98)  
**Getopt::Long** (latest version)  
**Encode::Locale** (latest version)  
**Encode::Byte** (latest version)  
**MIME::Base64** (latest version)  
**IO::Socket::SSL** (latest version)  
**WWW::Mechanize** (latest version)  
**File::Slurp** (latest version)  
**JSON::Parse** (latest version)  
**Text::Table** (latest version)  
**Data::Dumper** (latest version)  

# Help start_vro_workflow.pl:

```
Usage: start_vro_workflow.pl [-s|--server <vRO Server>] [-l|--port <vRO TCP Port>] [-u|--username <vRO username>] [-p|--password <vRO password>] -i|--id <vRO workflow ID> 
							 [-c|--content <JSON content request>] [-t|--timeout <seconds>] [-d|--debug] [-v|--version] [-h|--help]

        -s | --server
                vRO Server
        -l | --port
                vRO TCP Port
        -u | --username
                vRO Username
        -p | --password
                vRO Password
        -i | --id
                vRO Workflow ID
        -c | --content
                Load file containing content request in JSON format (default content = { "parameters": [] })
        -t | --timeout
                Timeout request HTTP (default = 60 sec)
        -d | --debug
                Enable debug HTTP/HTTPS protocol
        -v | --version
                Show robot version
        -h | --help
                This help

Examples:

(1) start_vro_workflow.pl --id ae8b1040-67ff-433f-8f5b-b972541dd9b1 --content ./content_request.json --debug
(2) start_vro_workflow.pl --id ae8b1040-67ff-433f-8f5b-b972541dd9b1 --content ./content_request.json --server myvro02.mydomain.local --username "myuser03@mydomain.local" --password mypwd --debug
(3) start_vro_workflow.pl --id ae8b1040-67ff-433f-8f5b-b972541dd9b1 --content ./content_request.json --username "myuser03@mydomain.local" --password mypwd
(4) start_vro_workflow.pl --version
(5) start_vro_workflow.pl --help
```
