#!/usr/bin/perl -w
#
# $Id: start_vro_workflow.pl v1.0.0 28/07/2017 17:00:00 cilo <danilo.cilento@gmail.com> $
#
#


# Load modules
use strict;
use Getopt::Long;
use Encode::Locale;
use Encode::Byte;
use MIME::Base64;
use IO::Socket::SSL;
use WWW::Mechanize;
use File::Slurp;
use JSON::Parse ':all';
use Text::Table;
use Data::Dumper;

my ($opt_host, $opt_port, $opt_username, $opt_password, $opt_workflow_id, $opt_content, $opt_timeout, $opt_debug, $opt_version, $opt_help) = ('myvro.mydomain.local', 8281, 'cloudadmin@mydomain.local', 'mypassword', '', '', 60, 0, 0, 0);
my $options = GetOptions (
	's|server=s'			=> \$opt_host,
	'l|port=i'				=> \$opt_port,
	'u|username=s'			=> \$opt_username,
	'p|password=s'			=> \$opt_password,
	'i|id=s'				=> \$opt_workflow_id,
	'c|content=s'			=> \$opt_content,
	't|timeout=i'			=> \$opt_timeout,
    'd|debug'    			=> \$opt_debug,
    'v|version'    			=> \$opt_version,
    'h|help' 	 			=> \$opt_help
);

if ($opt_help) {
	&help;
}

if ($opt_version) {
	&version;
}

if ($opt_workflow_id eq "") {
	print "\n[ERROR] You must specify a workflow ID\n";
	&help;
}

my $json;
if ($opt_content eq "") {
	$json = '{ "parameters": [] }';
} else {
	if (! -e "${opt_content}") {
		print "\n[ERROR] Content request file ${opt_content} does not exist\n";
		exit 1;
	} else {
		$json = read_file( "${opt_content}" ) ;
	}
}

eval { 
	assert_valid_json ($json);
};
if ($@) {
	print "\n[ERROR] Content request in JSON format is invalid: $@\n";
	exit 1;
} else {
	if ($opt_debug) {
		print "\n[DEBUG] Content request in JSON format is well formed\n";
	}
}

if ($opt_debug) {
    &debug;		# enable debug HTTP/HTTPS (request/response)
}

# HTTP robot environment
my $robot = '';
my $url = "https://${opt_host}:${opt_port}/vco/api/workflows/${opt_workflow_id}/executions/";
my $authBase64 = encode_base64("${opt_username}:${opt_password}");
my $status = '';
my $response = '';
my $runid = ''; 
my $wfresult = '';

if ($opt_debug) {
	print "[DEBUG] VMware vRO execution data:\n\n";
    my $tb = Text::Table->new(
        "|SERVER", "|PORT", "|USERNAME", "|PASSWORD", "|WORKFLOW ID"
    );
    $tb->load(
        [ "|${opt_host}", "|${opt_port}", "|${opt_username}", "|${opt_password}", "|${opt_workflow_id}" ]
    );
    print $tb . "\n";
}

# Create robot object
$robot = WWW::Mechanize->new(
	ssl_opts => {
		SSL_verify_mode => IO::Socket::SSL::SSL_VERIFY_NONE,
		verify_hostname => 0
	},
	keep_alive  => 1, 
    agent       => 'VRORobot/1.00', 
    autocheck   => 0,
    timeout     => $opt_timeout
);

$robot->add_header( "Authorization" => "Basic ${authBase64}" );
$robot->add_header( "Accept" => "application/json" );
$robot->add_header( "Content-Type" => "application/json" );

if ($opt_debug) {
	print "[DEBUG] :: Start workflow with ID ${opt_workflow_id} ::\n";
	&inforeq("POST", $url);
}

$robot->post($url, Content => $json);
$status = $robot->status();

unless ($robot->success()) {
	print "\n[ERROR] Cannot get URL: '${url}' (HTTP Code: ${status})\n";
    exit 1;
}

if ($status == 202) {
	$response = $robot->content();

	if ($opt_debug) {
		print "http-body-response:\n\n";
		print "${response}\n\n";
	}
} else {
	print "\n[ERROR] HTTP status code is not 202 (HTTP Code: ${status})\n";
    exit 1;
}

$url = $robot->response()->header( 'Location' );

if ($url =~ /https:\/\/.+\/executions\/(.+)\//) {
	$runid = $1;
}

do {
	if ($opt_debug) {
		print "[DEBUG] :: Get state of the workflow with ID ${opt_workflow_id} (run ID: ${runid}) ::\n";
		&inforeq("GET", "${url}state");
	}

	$robot->get("${url}state");
	$status = $robot->status();

	unless ($robot->success()) {
		print "\n[ERROR] Cannot get URL: '${url}' (HTTP Code: ${status})\n";
		exit 1;
	}

	if ($status == 200) {
		$response = $robot->content();

		if ($opt_debug) {
			print "http-body-response:\n\n";
			print "${response}\n\n";
		}
		
		$wfresult = parse_json ($response);
		if ($wfresult->{"value"} eq "running") {
			sleep(10);
		}
		if ($wfresult->{"value"} eq "waiting") {
			sleep(10);
		}
		if ($wfresult->{"value"} eq "failed") {
			print "\n[ERROR] Workflow with ID ${opt_workflow_id} is failed\n";
			exit 1;
		}
		if ($wfresult->{"value"} eq "canceled") {
			print "\n[ERROR] Workflow with ID ${opt_workflow_id} have been canceled\n";
			exit 1;
		}
	} else {
		print "\n[ERROR] HTTP status code is not 200 (HTTP Code: ${status})\n";
		exit 1;
	}
} while ($wfresult->{"value"} ne "completed");

if ($opt_debug) {
	print "[DEBUG] :: Get result of the workflow with ID ${opt_workflow_id} (run ID: ${runid}) ::\n";
	&inforeq("GET", $url);
}

$robot->get($url);
$status = $robot->status();

unless ($robot->success()) {
	print "\n[ERROR] Cannot get URL: '${url}' (HTTP Code: ${status})\n";
	exit 1;
}

if ($status == 200) {
	$response = $robot->content();

	if ($opt_debug) {
		print "http-body-response:\n\n";
		print "${response}\n\n";
	}
	
	$wfresult = parse_json ($response);
	print "\nvRO Workflow Result:\n\n";
	print Dumper($wfresult->{"output-parameters"});
} else {
	print "\n[ERROR] HTTP status code is not 200 (HTTP Code: ${status})\n";
	exit 1;
}

exit 0;


# Subroutine=============================================================================================

#
# Help command line
#
sub help {
    print "\nUsage: start_vro_workflow.pl [-s|--server <vRO Server>] [-l|--port <vRO TCP Port>] [-u|--username <vRO username>] [-p|--password <vRO password>] -i|--id <vRO workflow ID> [-c|--content <JSON content request>] [-t|--timeout <seconds>] [-d|--debug] [-v|--version] [-h|--help]\n\n";
	print "\t-s | --server\n";
	print "\t\tvRO Server\n";
	print "\t-l | --port\n";
	print "\t\tvRO TCP Port\n";
	print "\t-u | --username\n";
	print "\t\tvRO Username\n";
	print "\t-p | --password\n";
	print "\t\tvRO Password\n";
	print "\t-i | --id\n";
	print "\t\tvRO Workflow ID\n";
	print "\t-c | --content\n";
	print "\t\tLoad file containing content request in JSON format (default content = { \"parameters\": [] })\n";
	print "\t-t | --timeout\n";
	print "\t\tTimeout request HTTP (default = 60 sec)\n";
    print "\t-d | --debug\n";
    print "\t\tEnable debug HTTP/HTTPS protocol\n";
    print "\t-v | --version\n";
    print "\t\tShow robot version\n";
    print "\t-h | --help\n";
    print "\t\tThis help\n\n";
    print "Examples:\n\n";
    print "(1) start_vro_workflow.pl --id ae8b1040-67ff-433f-8f5b-b972541dd9b1 --content ./content_request.json --debug\n";
    print "(2) start_vro_workflow.pl --id ae8b1040-67ff-433f-8f5b-b972541dd9b1 --content ./content_request.json --server myvro02.mydomain.local --username \"myuser03\@mydomain.local\" --password mypwd --debug\n";
    print "(3) start_vro_workflow.pl --id ae8b1040-67ff-433f-8f5b-b972541dd9b1 --content ./content_request.json --username \"myuser03\@mydomain.local\" --password mypwd\n";
    print "(4) start_vro_workflow.pl --version\n";
    print "(5) start_vro_workflow.pl --help\n\n";
    exit 0;
}

#
# Debug HTTP/HTTPS protocol
#
sub debug {
    use Aspect; 

    my $pcut = call qr/LWP::UserAgent::send_request/; 

    before { 
        my $ctx = shift; 
        print "http-req====================\n"; 
        print $ctx->params->[1]->as_string; 
    } $pcut; 

    after { 
        my $ctx = shift; 
        print "http-resp-------------------\n"; 
        print $ctx->return_value->headers->as_string; 
        print "============================\n\n";
    } $pcut;
}

#
# Show data http request
#
sub inforeq {
	my @auth_data = split(/:/, decode_base64($authBase64));
	
	print "[DEBUG] (HTTP " . $_[0] . ") URL: " . $_[1] . "\n";
	print "[DEBUG] (HTTP " . $_[0] . ") Basic Authorization (Base64 encoded): " . $authBase64;
	print "[DEBUG] (HTTP " . $_[0] . ") Username (decoded): " . $auth_data[0] . "\n";
	print "[DEBUG] (HTTP " . $_[0] . ") Password (decoded): " . $auth_data[1] . "\n";
	if ($_[0] eq "POST") {
		print "[DEBUG] (HTTP " . $_[0] . ") Content request: " . $json . "\n";
	}
	print "\n";
}

#
# Show robot version
#
sub version {
    print "\n\$Id: start_vro_workflow.pl v1.0.0 28/07/2017 17:00:00 cilo <danilo.cilento\@gmail.com> \$\n";
    exit 0;
}
