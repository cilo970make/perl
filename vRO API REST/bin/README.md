# Build executable package with the following dll:

* libeay32__.dll
* ssleay32__.dll
* zlib1__.dll

```pp.bat ..\src\start_vro_workflow.pl -o start_vro_workflow.exe -l C:\strawberry-perl-5.22.0.1\c\bin\libeay32__.dll -l C:\strawberry-perl-5.22.0.1\c\bin\ssleay32__.dll -l C:\strawberry-perl-5.22.0.1\c\bin\zlib1__.dll```

[Perl module PAR](http://search.cpan.org/~rschupp/PAR-1.015/lib/PAR.pm) (Perl Archive Toolkit) is required to build executable package.

# Start my workflow "Sync all subscribed librarys in all vCenter" defined in VMware vRO

* with debug:

```C:\> start_vro_workflow.exe --id f14231dd-222f-4e4e-adb4-316cb854d57a --debug```

* without debug:

```C:\> start_vro_workflow.exe --id f14231dd-222f-4e4e-adb4-316cb854d57a```
